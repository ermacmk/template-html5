Bootstrap/HTML5 Responsive Template

Excell responsive html5 template is a multi-purpose website template with a modern clean designed build on top of Bootstrap using HTML5 CSS3 this template fits in all types of domains like portfolio, corporate, business, agency, consultancy, real estate, interior designer, and many more. This template designed with height quality standards to meet the latest requirements and it is a responsive template fits in all devices with multi browser support. Download for free.  

Credits :
-------
=> Design and developed: "WebThemez"  http://webthemez.com
=> Photos used in template: **Unsplash** - http://unsplash.com
=> For more free web themes: http://webthemez.com
=> Framework : http://getbootstrap.com

Important Note:
---------------
To remove backlink from the template, you need to donate / buy to remove the backlink from the template.
Any question contact us: webthemez@gmail.com


License :
-------
**Creative Commons Attribution 3.0** - http://creativecommons.org/licenses/by/3.0/

- You are allowed to use all files for both personal and commercial projects.

- If you use/modify the resources in your projects,we�d appreciate a linkback to this site.

- You do not have rights to redistribute,resell or offer files from this site to any third party

- If you wish to remove backlink from the template, you need to buy or donate min USD $10 to remove backlink (credits) form the template

- If you have any question,feel free to contact us at webthemez@gmail.com

- All images user here is for demo purpose only, we are not responsible for any copyrights.
